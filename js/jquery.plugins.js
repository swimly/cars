/**
 * Created by 97974 on 2016/5/25.
 */
(function($) {
    $.fn.extend({
        //首页banner插件
        Pop: function(options) {
            //设置默认值
            var defaults={
                effect:"click",
                popObj:".pop",
                mask:true,
                maskname:"pop_mask",
                popBox:"#pop1",
                closeBtn:true
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var _this=$(this);
                var mask=null;
                var close=null;
                _this.bind(options.effect,showPop)
                function showPop(){
                    //时候加载遮罩层
                    var mask=options.mask?createMask():null;
                    show();
                }
                /*创建遮罩*/
                function createMask(){
                   mask=$('<div class="'+options.maskname+'"></div>').appendTo($(document.body));
                }
                function show(){
                    $(options.popBox).show();
                    center($(options.popBox))
                    addCloseBtn();
                }
                function hide(){
                    $(options.popBox).hide();
                    mask.hide().remove();
                }
                function center(obj){
                    var width=obj.width();
                    var height=obj.height();
                    obj.css({
                        marginLeft:-width/2,
                        marginTop:-height/2
                    })
                }
                function addCloseBtn(){
                    close=$('<a class="close" href="javascript:;"></a>').appendTo(options.popBox);
                    close.bind("click",hide);
                }
            });
        },
        Star:function(options) {
            //设置默认值
            var defaults = {
                effect:"click",
                edit:true,//为true就是可以点击，否则仅作展示用！
                size:25,
                off:"img/star-off.png",
                on:"img/star-on.png"
            }
            var options = $.extend(defaults, options);
            //遍历匹配元素的集合
            return this.each(function () {
                var _this = $(this);
                var star=null;
                var total=_this.attr("total");
                var current=_this.attr("value");
                init(current,total);
                _this.css({height:options.size,verticalAlign:"middle"})
                var cursor=options.edit?(function(){
                    _this.children().css({cursor:"pointer"});
                })():null;
                _this.children().bind(options.effect,function(){
                    var clickH=options.edit?clickHandle($(this)):null;
                })
                function clickHandle(_this){
                    current=_this.index()+1;
                    change(current);
                    console.log(current)
                    return current;
                }
                function init(c,t){
                    for(var i=0;i<t;i++){
                        star=$('<i></i>').appendTo(_this);
                        star.css({
                            width:options.size,
                            height:options.size,
                            display:"inline-block",
                            "background-position":"center center",
                            "background-repeat":"no-repeat",
                            "background-image":"url("+options.off+")"
                        })
                    }
                    change(c)
                }
                function change(c){
                    _this.find("i").css({
                        "background-image":"url("+options.off+")"
                    });
                    for(var i=0;i<c;i++){
                        _this.find("i").eq(i).css({
                            "background-image":"url("+options.on+")"
                        });
                    }
                }
            });
        },
        Slider:function(options){
            var defaults = {
                effect:"click",
                thumb:false,
                numNav:false,
                title:false,
                irrow:false,
                speed:2000,
                data:[{
                    img:"img/img003.jpg",
                    title:"途悦汽车养护美容服务专家1",
                    url:"#"
                },{
                    img:"img/img004.jpg",
                    title:"途悦汽车养护美容服务专家2",
                    url:"#"
                },{
                    img:"img/img005.jpg",
                    title:"途悦汽车养护美容服务专家3",
                    url:"#"
                },{
                    img:"img/img006.jpg",
                    title:"途悦汽车养护美容服务专家3",
                    url:"#"
                }]
            }
            var options = $.extend(defaults, options);
            //遍历匹配元素的集合
            return this.each(function () {
                var _this = $(this);
                var imgWrap=null;
                var current=0;
                var length=options.data.length;
                var step=_this.width();
                var timer=null;
                var thumb=null;
                var num=null;
                var title=null;
                init();
                function init(){
                    imgWrap=$('<ul class="slider_wrap"></ul>').appendTo(_this);
                    imgWrap.css({
                        width:step*length
                    })
                    for(var i=0;i<length;i++){
                        $('<li><a href="'+options.data[i].url+'"><img src="'+options.data[i].img+'"/></a></li>').appendTo(imgWrap)
                    }
                    var titleShow=options.title?addTitle():null;
                    var numNavShow=options.numNav?addNumNav():null;
                    var thumbShow=options.thumb?addThumb():null;
                    changeThumb(current)
                }
                function addTitle(){
                    title=$('<div class="slider_title">'+options.data[current].title+'</div>').appendTo(_this);
                }
                function addNumNav(){
                    num=$('<div class="slider_numNav">'+(current+1)+'/'+length+'</div>').appendTo(_this)
                }
                function addThumb(){
                    thumb=$('<ul class="slider_thumb"></ul>').appendTo(_this);
                    var thumbBtn=null;
                    for(var i=0;i<length;i++){
                        thumbBtn=$('<li><img src="'+options.data[i].img+'"/></li>').appendTo(thumb)
                        thumbBtn.bind(options.effect,function(){
                            clearInterval(timer)
                            current=$(this).index();
                            move(current)
                            setTimeout(function(){
                                clearInterval(timer)
                                timer=setInterval(change,options.speed);
                            },0)
                        })
                    }
                }
                function move(idx){
                    imgWrap.css({left:-idx*step});
                    changeThumb(idx);
                    upDateNum();
                    upDateTitle();
                }
                function upDateNum(){
                    num.html((current+1)+'/'+length)
                }
                function upDateTitle(){
                    title.html(options.data[current].title)
                }
                function changeThumb(i){
                    thumb.children().eq(i).addClass("current").siblings().removeClass("current");
                }
                timer=setInterval(change,options.speed);
                function change(){
                    if(current<length-1){
                        current++;
                    }else{
                        current=0;
                    }
                    console.log(current);
                    move(current)
                }
            });
        }
    });
})(jQuery);