$(function(){
	$('#con_hd').on('click','.hd_t',function(){
		$(this).addClass('on').siblings('.hd_t').removeClass('on');
		$('#con_bd .abou_more').eq($(this).index()).show().siblings().hide();
	})
	$('.slide_prew').on('click','img',function(){
		$(this).addClass('current').siblings('img').removeClass('current');
		$('.slide').css('left',-394*$(this).index());
	})
	$('#close').click(function(){
		$(this).parent().remove();
	})
	$(window).on('scroll',function(){
		if(!$('.wel').length) return;
		var iScroll = $(document).scrollTop();
		if(iScroll-310>=150){
			$('.wel').addClass('fixed')
		}else{
			$('.wel').removeClass('fixed')
		}
	})
	$(window).on('resize',function(){
		if($(window).width()<1000){
			$('.wel').addClass('right');
		}else{
			$('.wel').removeClass('right');
		}
	})
	$('.table-title').on('click','h2',function(){
		$(this).addClass('active').siblings().removeClass('active');
		$('.table-form').eq($(this).index()).show().siblings('.table-form').hide();
	});
	/*查看更多车辆*/
	$(".show_more").Pop();
	$(".star").Star({edit:false});
	$(".star-big").Star({edit:false,size:35,off:"img/star-off-b.png",on:"img/star-on-b.png"});
})



